import java.util.Scanner;

public class ImcClaseMath {
    static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {

        // Declaración de variables
        int edad;
        double peso, estatura, calculo;

        System.out.println("----- Calculadora -----");

        //Entrada de datos por consola
        System.out.println("Ingrese su edad");
        edad = s.nextInt();
        System.out.println("Ingrese su estatura en metros, por ejemplo: '1,80'");
        estatura = s.nextDouble();
        System.out.println("Ingrese su peso en kilográaos: ");
        peso = s.nextDouble();

        // Lógica para calcular el IMC

        // Cálculo de imc
        calculo = peso/Math.pow(estatura,2);

        // Sentencia if, con salida de datos para determinar el nivel en el que se encuentra según IMC
        if(calculo < 22 && edad < 45)
            System.out.println("Riesgo bajo");
        if((calculo < 22 && edad >=45) || (calculo >= 22 && edad <=45))
            System.out.println("Riesgo medio");
        if(calculo >= 22 && edad >=45)
            System.out.println("Riesgo alto");

        // Fin método main
    }
}






