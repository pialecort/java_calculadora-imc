import java.util.Scanner;

public class Imc {
    static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {

        // Declaración de variables
        int edad;
        double peso, estatura, calculo;

        System.out.println("----- Calculadora -----");

        //Entrada de datos por consola
        System.out.println("Ingrese su edad");
        edad = s.nextInt();
        System.out.println("Ingrese su estatura en metros, por ejemplo: '1,80'");
        estatura = s.nextDouble();
        System.out.println("Ingrese su peso en kilogramos: ");
        peso = s.nextDouble();

        // Lógica para calcular el IMC

        // Cálculo imc
        calculo = peso /(estatura * estatura);

        // Sentencia if, con salida de datos para determinar el nivel en el que se encuentra según IMC

        if(calculo < 22 && edad < 45){
            System.out.println("El riesgo de obesidad es bajo, su IMC es de: " + calculo);
        }else if ((calculo < 22 && edad >= 45) || ( calculo >= 22 && edad <= 45)){
            System.out.println("El riesgo de obesidad es medio, su IMC es de: " + calculo);
        }else if (calculo >= 22 &&edad >= 45 ){
            System.out.println("Usted está con sobre peso riesgoso, su IMC es de: "+ calculo);
        }

        // Fin método main
    }
}
